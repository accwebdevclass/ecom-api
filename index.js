var jsonServer = require('json-server');
var server = jsonServer.create();
var router = jsonServer.router('./db.json');
var middlewares = jsonServer.defaults();
var port = process.env.PORT || 4444;

var filterFieldsMiddleware = require('./filter-fields');
var routes = require('./routes.json');

server.use(middlewares);
server.use(filterFieldsMiddleware);
server.use(jsonServer.rewriter(routes));
server.use(router);

server.listen(port, function () {
  console.log('JSON Server is running');
})

module.exports = router;