const _ = require('lodash');

module.exports = (req, res, next) => {
	const _send = res.send
	res.send = function (body) {
		try {
		    const json = JSON.parse(body)
		    if (Array.isArray(json) && json.length > 1) {
		    	const data = json.map(item => 
		    		_.mergeWith(
		    			_.omit(item, [
			    			'images', 
			    			'product_brand', 
			    			'variation_attributes', 
			    			'child_items', 
			    			'rating_distribution', 
			    			'bullet_description', 
			    			'soft_bullets', 
			    			'wellness_merchandise_attributes', 
			    			'sales_classification_nodes', 
			    			'child_items', 
			    			'sponsored_search', 
			    			'top_reviews', 
			    			'secondary_ratings_averages', 
			    			'secondary_ratings_averages_order', 
			    			'promotions'
			    		]),
			    		{ image: _.get(item, 'images[0].base_url') + _.get(item, 'images[0].primary') }
			    	)
		    	)
		    	return _send.call(this, JSON.stringify(data))
		    }
		} catch (e) {}
		return _send.call(this, body)
	}
	next()
}