[https://hpwd-ecom-api.herokuapp.com/]

## Scripts
- *dev*:       Run local dev server
- *start*:     Required for Heroku

# REST API Endpoints

## Products
- /api/v1/item/:id
- /api/v1/search/:keywords
- /api/v1/items/page/:page/limit/:limit
- /api/v1/items/page/:page/limit/:limit/sort/featured
- /api/v1/items/page/:page/limit/:limit/sort/price-low-to-high
- /api/v1/items/page/:page/limit/:limit/sort/price-high-to-low
- /api/v1/items/page/:page/limit/:limit/sort/ratings
- /api/v1/filter/brand/:brand
- /api/v1/filter/color/:color
- /api/v1/filter/size/:size
- /api/v1/filter/brand/:brand/color/:color
- /api/v1/filter/brand/:brand/size/:size
- /api/v1/filter/color/:color/size/:size
- /api/v1/filter/brand/:brand/color/:color/size/:size

## Facets
- /api/v1/facets

## Options
- /api/v1/options/sort
